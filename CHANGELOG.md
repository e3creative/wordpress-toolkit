# Changelog

## [2.0.0] - 2019-10-31
### Changed
- Updates vendor name

## [1.2.1] - 2018-09-13
### Removed
- Null coalesceoperators to make this compatible with PHP 5.6

## [1.2.0] - 2018-09-13
### Removed
- Scalar type hints, return typhints and collapsed ternaries to make this compatible with PHP 5.6

## [1.0.1] - 2018-09-13
### Fixed
- Composer package name

## [1.0.0] - 2018-09-13
### Initial release
