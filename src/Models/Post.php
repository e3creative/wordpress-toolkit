<?php

namespace E3Creative\WordpressToolkit\Models;

class Post
{
    /**
     * @var mixed
     */
    public $id;

    /**
     * @var WP_Post
     */
    protected $post;

    /**
     * @var array
     */
    protected $meta;

    /**
     * @param mixed $id
     */
    public function __construct($id = null)
    {
        $this->id = $id ?: get_the_id();
    }

    /**
     * Get post property or empty Value
     *
     * @param  string $name
     * @return array|int|string
     */
    public function __get($name)
    {
        $method = 'get' . ucfirst($name);

        if (!$this->post) {
            $this->post = $this->getPostObject();
        }

        return $this->$method();
    }

    /**
     * Retrieves a meta value or ACF field value. Allows dot notaion for delving
     * into arrays, e.g. repeater fields, tabs or field groups
     * @param  string $name
     * @return array|int|string
     */
    public function field($name)
    {
        if (!$this->meta[$name]) {
            $parts = explode('.', $name);

            $this->meta[$name] = get_field(array_shift($parts), $id);

            $this->meta[$name] = array_reduce($parts, function ($carry, $index) {
                return isset($carry[$index]) ? $carry[$index] : $carry;
            }, $this->meta[$name]);
        }

        return $this->meta[$name];
    }

    /**
     * @return mixed
     */
    protected function getId()
    {
        return $this->id;
    }

    /**
     * Returns the featured image URL for this post
     *
     * @return string
     */
    protected function getTitle()
    {
        return $this->post->post_title;
    }

    /**
     * Gets the page permalink
     *
     * @return void
     */
    protected function getLink()
    {
        return get_the_permalink($this->id);
    }

    /**
     * Returns the author of this
     *
     * @return string
     */
    protected function getAuthor()
    {
        if (!$this->meta['author']) {
            $this->meta['author'] = get_the_author_meta('display_name', $this->post->post_author);
        }

        return $this->meta['author'];
    }

    /**
     * Returns published date for this post
     *
     * @return string
     */
    protected function getPublishedDate()
    {
        return $this->post->post_date;
    }

    /**
     * Returns the featured image URL for this post
     *
     * @return string
     */
    protected function getThumbnailUrl()
    {
        if ($thumbnail = get_the_post_thumbnail_url($this->id)) {
            return home_url() . $thumbnail;
        }
    }

    /**
     * Gets the page taxonomies
     *
     * @return void
     */
    public function getTaxonomies($type)
    {
        $taxonomies = wp_get_post_terms($this->id, ['taxonomy' => $type]);

        if (!is_array($taxonomies)) {
            return null;
        }

        return array_map(function ($term) {
            return $term->name;
        }, $taxonomies);
    }

    /**
     * Returns the post for the given ID
     * @return WP_Post
     */
    private function getPostObject()
    {
        $post = get_post($this->id);

        if (!$post) {
            throw new PostNotFoundException("Your post could not be found");
        }

        return $post;
    }
}
