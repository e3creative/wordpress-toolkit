<?php

namespace E3Creative\WordpressToolkit\Exceptions;

use Exception;

class PostNotFoundException extends Exception
{
    protected $code = 204;
}
