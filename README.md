# Wordpress Toolkit

### Models/Post.php
Provides access to Post categories and meta/fields. Uses class property caching so multiple calls to a single field
or propery will only lookup the value in the database once.


```
$post = new Post; // Optionally pass ID as constructor arg if not retrieving the current post

$id = $post->id;                                // 1
$title = $post->title;                          // Birthday Party
$authorName = $post->author;                    // Joan Evans
$permalink = $post->link;                       // https://whatever.com/my-birthday-party
$thumbnailUrl = $post->thumbnailUrl             // https://whatever.com/wp-content/uploads/my-birthday-party.jpg

$categories = $post->getTaxonomies('category'); // [0 => 'events', 1 => 'insights']

$acfFields['name'] = $post->field('name');      // field() retrieves meta and ACF fields
$acfFields['name'] = $post->field('doc.name');  // use dot notation to delve into ACF field groups, repeaters and arrays

```
